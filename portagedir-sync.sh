SYNCHOST="rsync://haibane"
HOSTFOLDER="/intel-stable/"
CLIENTFOLDER="/etc/portage/"
#CLIENTFOLDER="/home/chris/p-test/"

syncitem() {
	rsync -avhl --progress "${SYNCHOST}:${HOSTFOLDER}${1}" "${CLIENTFOLDER}${1}" --delete
}
syncitem 'source-files/'
syncitem 'package.use/'
syncitem 'package.accept_keywords/'
syncitem 'package.license'
syncitem 'package.mask/'
syncitem 'package.unmask/'
syncitem 'repos.conf/'
